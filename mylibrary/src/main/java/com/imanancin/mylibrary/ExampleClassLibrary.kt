package com.imanancin.mylibrary

import android.util.Log

class ExampleClassLibrary {
    private val TAG = "ExampleClassLibrary"

    fun helloFromLibrary() {
        Log.d(TAG, "Hello this message came from library class: " + ExampleClassLibrary::class.simpleName)
    }

}