package com.imanancin.testmodule

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.imanancin.mylibrary.ExampleClassLibrary

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // call method from library/module
        val exampleClassLibrary = ExampleClassLibrary()
        exampleClassLibrary.helloFromLibrary()
    }
}